import DropdownList from './components/dropdown-list/DropdownList';
import './App.css';

function App() {
  return (
    <div className="App">
      <DropdownList />
    </div>
  );
}

export default App;
