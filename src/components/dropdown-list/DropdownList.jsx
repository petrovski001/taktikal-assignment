import DropdownItem from "./dropdown-item/DropdownItem";
import { data } from "../../data/data";
import "./dropdown-list.css";
import { useState } from "react/cjs/react.development";
import { useEffect } from "react";

const KEY = {
  UP: 38,
  DOWN: 40,
  ENTER: 13,
};

export default function DropdownList() {
  const [search, setSearch] = useState("");
  const [matchedItems, setMatchItems] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [selectedItem, setSelectedItem] = useState(null);

  useEffect(() => {
    if (search === "") {
      setMatchItems([]);
      setSelectedItem(null);
    }
    if (search !== "") {
      const searchString = search.toLowerCase();
      const items = data.filter(
        (item) =>
          item.name.toLowerCase().includes(searchString) ||
          item.email.toLowerCase().includes(searchString)
      );
      if(selectedItem && search !== selectedItem.name) setSelectedItem(null);
      setMatchItems(items);
    }
  }, [search, selectedItem]);

  const handleChange = (event) => {
    const { value } = event.target;
    setSearch(value);
  };

  const handleItemClick = (item) => {
    setSearch(item.name);
    setSelectedItem(item);
  };

  const onArrowKeys = (event) => {
    const { keyCode } = event;
    if (keyCode === KEY.ENTER) {
      setSelectedItem(matchedItems[selectedIndex]);
      setSearch(matchedItems[selectedIndex].name);
      setSelectedItem(matchedItems[selectedIndex]);
    }
    if (keyCode === KEY.UP || keyCode === KEY.DOWN) {
      if (keyCode === KEY.DOWN && selectedIndex < matchedItems.length - 1) {
        setSelectedIndex((selectedIndex) => selectedIndex + 1);
      }
      if (keyCode === KEY.UP && selectedIndex > 0) {
        setSelectedIndex((selectedIndex) => selectedIndex - 1);
      }
    }
  };

  const renderMatchedItems = () => {
    return matchedItems.length ? (
      matchedItems.map((item, i) => (
        <DropdownItem
          key={item.ssn}
          item={item}
          itemIndex={i}
          search={search}
          selectedIndex={selectedIndex}
          handleClick={handleItemClick}
        />
      ))
    ) : (
      <DropdownItem item={null} search={search} />
    );
  };

  const render = () => {
    if (selectedItem) return;
    return (
      <>
        {search && <div className="dropdown-list">{renderMatchedItems()}</div>}
      </>
    );
  };
  return (
    <div>
      <input
        type="text"
        name="search"
        placeholder="Þekktir viðtakendur"
        value={search}
        onChange={handleChange}
        onKeyDown={onArrowKeys}
      />
      {render()}
    </div>
  );
}
