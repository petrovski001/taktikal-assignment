import "./dropdown-item.css";

export default function DropdownItem({item, itemIndex, search, selectedIndex, handleClick}) {
  if(item == null) {
    return <div className="item" key={1}>
      <p className="error">Engar niðurstöður</p>
    </div>
  }
  
  const boldQuery = (string) => {
    const n = string.toUpperCase();
    const q = search.toUpperCase();
    const x = n.indexOf(q);
    if (!q || x === -1) {
        return string;;
    }
    const l = q.length;
    return string.substr(0, x) + '<b>' + string.substr(x, l) + '</b>' + string.substr(x + l);
}

  const { ssn, name, email } = item;
  return (
    <div className={`item ${selectedIndex === itemIndex ? "active" : ""}`} onClick={(e) => handleClick(item)} key={ssn}>
      <p className="name" dangerouslySetInnerHTML={{__html: boldQuery(name)}} />
      <p className="email"dangerouslySetInnerHTML={{__html: boldQuery(email)}} />
    </div>
  );
}
