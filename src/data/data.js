export const data = [
    {
      "name": "Jón Þór Þórólfsson",
      "email": "jon@fyrirtaekjalausnir.is",
      "ssn": "1758101830"
    },
    {
      "name": "Geir Hannes Björnsson",
      "email": "geir@bankastarfsemi.com",
      "ssn": "2849033620"
    },
    {
      "name": "Geirþrúður Björk Gunnarsdóttir",
      "email": "geirthrudur@opinberstofnun.is",
      "ssn": "173800127"
    },
    {
      "name": "Haraldur Fjólmundssson",
      "email": "halli@fjarmalastofnun.is",
      "ssn": "2847003900"
    },
    {
      "name": "Birgitta Líf Brjánsdóttir",
      "email": "birgitta@aviato.is",
      "ssn": "17381205227"
    },
    {
      "name": "Gunnleifur Geirfinnsson",
      "email": "gunnig@bigcorpehf.is",
      "ssn": "1502038492"
    },
    {
      "name": "Birgir Ben Brjánsson",
      "email": "biggi@stofnanaeftirlitid.is",
      "ssn": "2758003830"
    }
  ]